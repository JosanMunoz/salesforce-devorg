trigger InventoryDeleteTrigger on Inventory__c (before delete) {

  // Check to see if there's general collection for orphaned items
  Inventory__c lostnfound;
  try {
    lostnfound = [SELECT Id,Name FROM Inventory__c WHERE Name='Lost and Found' LIMIT 1];
  } catch(QueryException e){
    // Create set if none found
    lostnfound = new Inventory__c(Name='Lost & Found');
    upsert lostnfound;
  }


  list<Inventory_Item__c> updatedItems = new List<Inventory_Item__c>();

  // Loop through inventories deleted to transfer
  //   items to general inventory set 'Lost & Found'
  for(Inventory__c i : trigger.old){
    
    
    for(Inventory_Item__c item : [SELECT Name,Quantity__c,Inventory__c 
                                    FROM Inventory_Item__c 
                                    WHERE Inventory__c = :i.id]){
      if(item.quantity__c != 0){
        item.inventory__c = lostnfound.id;
        updatedItems.add(item);
      }
    }
  }

  upsert updatedItems;
}