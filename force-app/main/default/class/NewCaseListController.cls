public class NewCaseListController {

    public list<Case> getNewCases() {
        list<Case> results = [
            SELECT ID,CaseNumber
            FROM Case
            WHERE Status = 'New'];

        return results;
    }
}