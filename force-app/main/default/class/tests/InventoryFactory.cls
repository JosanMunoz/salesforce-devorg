@isTest
public class InventoryFactory {
    /*
        @input: 
            Boolean insertObjs - whether to insert new objs or not. For Insert trigger testing
            Integer createCount - amount of inventories to create
    */
    public static list<Inventory__c> soloInventories(Boolean insertObjs, Integer createCount) {
        list<Inventory__c> result = new list<Inventory__c>();

        for(Integer i = 0; i < createCount; i++){
            Inventory__c newInv = new Inventory__c(Name='Test Inventory '+i);
            result.add(newInv);
        }

        if(insertObjs) {
            insert result;
        } 

        return result;
    }

    /*
        @input: 
            Integer inventoryCount - amount of inventories to create.
            Integer itemCount - amount of items each inventory will have.
    */
    public static list<Inventory__c> populatedInventories(Integer inventoryCount, Integer itemCount) {
        list<Inventory__c> inventories = InventoryFactory.soloInventories(true, inventoryCount);

        for(Inventory__c currentInv : inventories) {
            list<Inventory_Item__c> newiItems = InventoryItemFactory.testItems(currentInv, itemCount, true);
        }

        return inventories;
    }

    /*
        @input: 
            Boolean insertObjs - whether to insert new objs or not. For Insert trigger testing
            Integer depth - depth of the inventory tree to create. creates depth+1 amount of inventories
            Boolean withChildren - If the inventories should have children. Creates 3 childs each
    */
    public static list<Inventory__c> treeInventory(Boolean insertObjs, Integer depth, Boolean withChildren) {
        depth = Math.max(1, depth);
        list<Inventory__c> inventories = withChildren ? InventoryFactory.populatedInventories(depth+1, 3) :
                                                        InventoryFactory.soloInventories(true, depth+1);

        for(Integer index = 0; index < depth; index++){
            Inventory__c currentInv = inventories.get(index);
            Inventory__c childInv = inventories.get(index+1);

            childInv.Master_Inventory__c = currentInv.id;
        }

        update inventories;
        return inventories;
    }
}
