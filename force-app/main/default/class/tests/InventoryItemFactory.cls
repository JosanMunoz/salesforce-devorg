@isTest
public class InventoryItemFactory {
    /*
    Input:
        Integer ownerId - Associated owner Inventory
        Integer itemCount - Number of inventory items to return
        Boolean insertObjs - Whether or not the created items should be inserted
    */
    public static list<Inventory_Item__c> testItems(Inventory__c owner, Integer itemCount, Boolean insertObjs) {
        list<Inventory_Item__c> result = new list<Inventory_Item__c>();

        for(Integer i = 0; i < itemCount; i++){
            Inventory_Item__c newItem = new Inventory_Item__c(Name='Test Inventory Item '+i, Inventory__c=owner.id, Quantity__c=1);

            result.add(newItem);
        }

        if(insertObjs) {
            insert result;
        }

        return result;
    }
}
