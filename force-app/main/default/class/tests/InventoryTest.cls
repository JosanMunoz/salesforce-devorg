@isTest
private class InventoryTest {
    
    /*
        Test for orphaned items to be moved to
            general inventory, and 0 quantity items are deleted
    */
    @isTest
    public static void testInventoryDelete(){
        Integer inventoryCount = Math.round( (Math.random() * 20) + 1);
        Integer itemCount = Math.round( (Math.random() * 20) + 1);

        list<Inventory__c> inventories = InventoryFactory.populatedInventories(inventoryCount, itemCount);

        Test.startTest();
        delete inventories;
        Test.stopTest();

        Inventory__c lostfound = [SELECT Id,Name,Total_Quantity__c FROM Inventory__c WHERE Name='Lost & Found' LIMIT 1];

        System.assert(lostfound != null, 'Could Not Find Inventory named \'Lost & Found\'');
        System.assertEquals(lostfound.Total_Quantity__c, inventoryCount * itemCount, 'Total amount of children found in \'Lost & Found\' - ' + lostfound.Total_Quantity__c + ', was not total created - ' + (inventoryCount * itemCount));
    }
}
